package main

import (
	"fmt"
	"local/antoan/src/startengines"
	"time"
)

func browser_main() {

	// lo_client := startengines.ClientInfo{}

	// active_engine := startengines.Engine{}

	// active_engine.ClientInfo = lo_client

	// l := launcher.New()

	// // For more info: https://pkg.go.dev/github.com/go-rod/rod/lib/launcher
	// u := l.MustLaunch()

	// active_engine.LaunchInfo = l
	// active_engine.Url = u

	//_, cancel := context.WithCancel(context.Background())

	//defer cancel()

	messchan := make(chan string)

	browsers := startengines.CreatexBrowsers(3)

	fmt.Println(browsers)

	urls := startengines.StartAllBrowser(browsers)

	fmt.Println(urls)

	f := func() {
		startengines.EndApplication(messchan)
	}

	time.AfterFunc(30*time.Second, f)

	startengines.RunForever(messchan)

	// time.Sleep(30 * time.Second)

	// go func(can context.CancelFunc) {

	// 	//ctx.Background().Deadline()

	// 	time.AfterFunc(60*time.Second, can)

	// }(cancel)

}
