module local/antoan

go 1.15

require (
	github.com/go-rod/rod v0.101.8
	github.com/kr/pretty v0.2.0 // indirect
	github.com/sony/sonyflake v1.0.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gorm.io/driver/sqlite v1.1.6
	gorm.io/gorm v1.21.16
)
