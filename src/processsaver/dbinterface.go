package saver

type MoveDBInterface interface {
	SaveMove(action ActionInterface)
	Scan()
	Value()
}

type PointerMoveDBInterface interface {
	Scan()
	Value()

	SaveMove(pointeraction PointerActionInterface)
}

type MiddleDBMoveInterface interface {
	SaveMove(middleaction MiddleActionInterface)
	Scan()
	Value()
}

type EndMoveDBInterface interface {
	SaveMove(endaction EndActionInterface)
	Scan()
	Value()
}

type ActionDBInterface interface {
	SaveAction()
	Scan()
	Value()
}

type PointerActionDBInterface interface {
	SaveAction()
	Scan()
	Value()
}

type MiddleActionDBInterface interface {
	SaveAction()
	Scan()
	Value()
}

type EndActionDBInterface interface {
	SaveAction()
	Scan()
	Value()
}
