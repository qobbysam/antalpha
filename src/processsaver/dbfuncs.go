package saver

import (
	"fmt"
	db "local/antoan/src/processsaver/db"

	"gorm.io/gorm"
)

const (
	PointerMovestring = "pointer"
	MiddleMovestring  = "middle"
	EndMovestring     = "end"
)

const (
	ClickActionstring      = "click"
	NavigateActionstring   = "navigate"
	EnterTextActionstring  = "entertext"
	ScreenShotActionstring = "screenshot"
)

func CreateProcess(name string) (*db.ProcessTable, error) {

	newprocess := db.ProcessTable{Name: name}
	//results := DB.Create(&newprocess)

	// if results.Error != nil {

	// 	fmt.Println("failed to save processes")

	// 	return &newprocess, results.Error

	// }

	return &newprocess, nil
}

func CreateMoveTable(name string, parentprocessid, step int) (db.MoveTable, error) {

	newtable := db.MoveTable{

		Name:            name,
		StepOfProcess:   step,
		ParentProcessID: parentprocessid,
	}

	return newtable, nil

}

func CreateSelectorTable(parentId int) (*db.SelectorDataTable, error) {

	newtable := db.SelectorDataTable{

		ParentProcessID: uint(parentId),
		//ActionTableID:   uint(actionid),
	}

	return &newtable, nil
}

func CreateReturnDataTable(parentId int) (*db.ReturnDataTable, error) {

	newtable := db.ReturnDataTable{

		ParentProcessID: uint(parentId),
		//ActionTableID:   uint(actionid),
	}

	return &newtable, nil
}

func CreateRuntimeDataTable(parentId int) (*db.RuntimeDataTable, error) {

	newtable := db.RuntimeDataTable{

		ParentProcessID: uint(parentId),
		//ActionTableID:   uint(actionid),
	}

	return &newtable, nil
}

func CreateActionTable(parentId, selectorid, returndataid, runtimedataid, moveid int, name string) (*db.ActionTable, error) {

	newtable := db.ActionTable{
		Name:            name,
		ParentProcessID: uint(parentId),
		SelectorDataID:  uint(selectorid),
		ReturnDataID:    uint(returndataid),
		RuntimeDataID:   uint(runtimedataid),
		MoveTableID:     uint(moveid),
	}

	return &newtable, nil
}

func CreateNewProcess(name string, dbg gorm.DB) error {

	newProcess, err := CreateProcess(name)

	processID, err = newProcess.Save(&dbg)

	if err != nil {
		return err
	}

	return nil
}

func AddmoveToProcess(movename string, step int, actionname string, parentprocessid int, dbg gorm.DB) error {

	parentprocess := db.ProcessTable{}

	results := dbg.First(&parentprocess, parentprocessid)

	if results.Error != nil {
		fmt.Println("results not found")

		return results.Error
	}

	newmove, err := CreateMoveTable(PointerMovestring, parentprocessid, step)

	if err != nil {

	}

	moveid, err := newmove.Save(dbg)

	newselector, err := CreateSelectorTable(parentprocessid)
	newreturndata, err := CreateReturnDataTable(parentprocessid)
	newruntimedata, err := CreateRuntimeDataTable(parentprocessid)

	selectorid, err := newselector.Save(dbg)

	returndataid, err := newreturndata.Save(dbg)

	runtimedataid, err := newruntimedata.Save(dbg)

	//returndataid , err :=

	newaction, err := CreateActionTable(parentprocessid, selectorid, returndataid, runtimedataid, moveid, NavigateActionstring)

	actionid, err := newaction.Save(dbg)

	fmt.Println("new action saved  ", actionid)

	return nil

}
