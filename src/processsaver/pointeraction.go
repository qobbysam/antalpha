package saver

type StartAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

type ChangeHeadAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

func (sa *StartAction) SaveAction() {

}
