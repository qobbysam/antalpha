package db

import "gorm.io/gorm"

type SelectorDataTable struct {
	ID              uint `gorm:"primaryKey"`
	ParentProcessID uint
	ActionTableID   uint
	SelectorData
}

func (st *SelectorDataTable) Save(db gorm.DB) (int, error) {

	results := db.Create(st)

	if results.Error != nil {

		return 0, results.Error
	}

	return int(st.ID), nil

}
