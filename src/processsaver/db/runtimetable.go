package db

import "gorm.io/gorm"

type RuntimeDataTable struct {
	ID              uint `gorm:"primaryKey"`
	ParentProcessID uint
	ActionTableID   uint
	RuntimeData
}

func (rt *RuntimeDataTable) Save(db gorm.DB) (int, error) {

	results := db.Create(rt)

	if results.Error != nil {

		return 0, results.Error
	}

	return int(rt.ID), nil

}
