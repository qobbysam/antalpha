package db

import "gorm.io/gorm"

type MoveTable struct {
	ID              uint `gorm:"primaryKey"`
	Name            string
	StepOfProcess   int
	ParentProcessID int
	ActionID        int
	ActionTable     ActionTable `gorm:"foreignKey:ActionID"`
	//ProcessTable ProcessTable
	//ProcessTable    ProcessTable `gorm:"foreignKey:CompanyRefer"`
}

func (mt *MoveTable) Save(db gorm.DB) (int, error) {

	results := db.Create(mt)

	if results.Error != nil {

		return 0, results.Error
	}

	return int(mt.ID), nil

}
