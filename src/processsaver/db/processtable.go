package db

import "gorm.io/gorm"

type ProcessTable struct {
	ID        uint `gorm:"primaryKey"`
	Name      string
	StepCount int
	MoveTable []MoveTable `gorm:"foreignKey:ID"`
}

func (pt *ProcessTable) Save(db *gorm.DB) (int, error) {

	results := db.Create(pt)

	if results.Error != nil {

		return 0, results.Error
	}

	return int(pt.ID), nil
}
