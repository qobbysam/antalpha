package db

import "gorm.io/gorm"

type ActionTable struct {
	ID              uint `gorm:"primaryKey"`
	Name            string
	ParentProcessID uint
	MoveTableID     uint
	SelectorDataID  uint
	ReturnDataID    uint
	RuntimeDataID   uint

	SelectorDataTable SelectorDataTable `gorm:"foreignKey:SelectorDataID"`
	ReturnDataTable   ReturnDataTable   `gorm:"foreignKey:ReturnDataID"`
	RuntimeDataTable  RuntimeDataTable  `gorm:"foreignKey:RuntimeDataID"`
}

func (at *ActionTable) Save(db gorm.DB) (int, error) {

	results := db.Create(at)

	if results.Error != nil {

		return 0, results.Error
	}

	return int(at.ID), nil
}
