package db

import "gorm.io/gorm"

type ReturnDataTable struct {
	ID              uint `gorm:"primaryKey"`
	ParentProcessID uint
	ActionTableID   uint
	ReturnData
}

func (rt *ReturnDataTable) Save(db gorm.DB) (int, error) {

	results := db.Create(rt)

	if results.Error != nil {

		return 0, results.Error
	}

	return int(rt.ID), nil

}
