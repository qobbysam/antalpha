package db

type SelectorData struct {
	//Action            Action
	XpathSelector     string
	CssPathSelector   string
	ClassNameSelector string
	IdNameSelector    string
	PositionSelector  string
}

type RuntimeData struct {
	//Action     Action
	Value      string
	ValuesList string
	//ValuesMap  map[string]interface{}
}

type ReturnData struct {
	//Action     Action
	Location   string
	ReturnData string
}
