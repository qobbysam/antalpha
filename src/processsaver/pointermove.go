package saver

type PointerMoveSolo struct {
	Name   string
	ID     int
	Action PointerActionInterface
}

type PointerMoveMultiple struct {

	//run against can go here.
	Name   string
	ID     int
	Action []PointerActionInterface
}

func CreatePointerMoveSolo(name string, id int, pointeraction string) *PointerMoveSolo {

	outmove := PointerMoveSolo{Name: name, ID: id}

	switch pointeraction {
	case "input":
		var action MiddleActionInterface = CreateInputAction(pointeraction)
		//newinputaction := CreateInputAction("input")
		//outaction := reflect.ValueOf(newinputaction).Interface().(MiddleActionInterface)
		outmove.Action = action
		return &outmove
	case "Check":

		return &outmove

	default:
		return &outmove

	}

}
