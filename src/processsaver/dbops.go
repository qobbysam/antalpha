package saver

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB, err = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})

type DBAction ActionDBInterface

type DBMove MoveDBInterface

type DBPointerMove PointerMoveDBInterface

type DBPointerAction PointerActionInterface

type DBMiddleAction MiddleActionInterface

type DBMiddleMove MiddleMoveInterface

type DBEndAction EndActionInterface

type DBEndMove EndMoveInterface

type DBprocessTemplate struct {
	Name  string
	ID    int
	Moves []DBMove
}

// func (dbm *DBMove) GetSelectorSpaces() SelectorData{
// 	outSelector := SelectorData{}

// 	action := dbm.GetAction()

// 	selectorData := GenerateSelectorData(action)

// 	outSelector

// }

func (db *DBprocessTemplate) AppendMoveEnd(move MoveDBInterface) {

	db.Moves = append(db.Moves, move)
}

func (db *DBprocessTemplate) AppendMoveFront(move MoveDBInterface) {
	moves := db.Moves

	newmoves := moves[1:]

	newmoves = append(newmoves, move)

	db.Moves = newmoves
}

// func (db *DBprocessTemplate) GetSelectorSpaces() []SelectorData {

// 	outSelectors := []SelectorData{}

// 	for _, v := range db.Moves {

// 		outSelectors = append(outSelectors, v.GetSelectorSpaces())

// 	}

// }

func CreateDBprocessTemplate(name string, id int) DBprocessTemplate {

	outdbprocess := DBprocessTemplate{Name: name, ID: id}

	return outdbprocess

}

// type DBprocessinstance struct {
// 	Name string
// 	ID int
// 	Moves []DBMoveinstance
// }

//parent name
//parent move
//parent action

// type DBMoveinstance struct {
// 	ID     int
// 	MoveType string
// 	Move DBMove
// 	Action DBAction
// }

// type DBActionInstance struct {
// 	ID           int
// 	Action DBAction
// }

// type DBProcess struct {
// 	Name  string
// 	ID    int
// 	Moves []DBMove
// }

// type DBSelectorData struct {
// 	Name string
// 	ID   int
// }

// type DBRuntimeData struct {
// 	Name string
// 	ID   int
// }

// type DBReturnData struct {
// 	Name string
// 	ID   int
// }

// type DBProcessInData struct {
// 	ProcessName  DBProcess
// 	RuntimeData  []DBReturnData
// 	SelectorData []DBSelectorData
// }

// type DBProcessOutData struct {
// 	ProcessName DBProcess
// 	ReturnData  []ReturnData
// }
