package saver

type MiddleMoveSolo struct {

	//think I need only id here.
	Name   string
	ID     int
	Action MiddleActionInterface
}

type MiddleMoveMultiple struct {
	Name    string
	Id      string
	Actions []MiddleActionInterface
}

func CreateMiddleMoveSolo(name string, id int, actionname string) *MiddleMoveSolo {

	outmove := MiddleMoveSolo{Name: name, ID: id}

	switch actionname {
	case "input":
		var action MiddleActionInterface = CreateInputAction(actionname)
		//newinputaction := CreateInputAction("input")
		//outaction := reflect.ValueOf(newinputaction).Interface().(MiddleActionInterface)
		outmove.Action = action
		return &outmove
	case "Check":

		return &outmove

	default:
		return &outmove

	}

}
