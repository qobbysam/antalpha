package saver

type MoveInterface interface {
	SaveMove(action ActionInterface)
}

type PointerMoveInterface interface {
	SaveMove()
}

type MiddleMoveInterface interface {
	SaveMove(middleaction MiddleActionInterface)
}

type EndMoveInterface interface {
	SaveMove(endaction EndActionInterface)
}

type ActionInterface interface {
	SaveAction()
}

type PointerActionInterface interface {
	SaveAction()
}

type MiddleActionInterface interface {
	SaveAction()
}

type EndActionInterface interface {
	SaveAction()
}
