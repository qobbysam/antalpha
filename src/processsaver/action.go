package saver

type Action struct {
	Name string
	ID   int
}

type SelectorData struct {
	//Action            Action
	XpathSelector     string
	CssPathSelector   string
	ClassNameSelector string
	IdNameSelector    string
	PositionSelector  string
}

type RuntimeData struct {
	//Action     Action
	Value      string
	ValuesList string
	//ValuesMap  map[string]interface{}
}

type ReturnData struct {
	//Action     Action
	Location   string
	ReturnData string
}

func (a *Action) Update(name string, ID int) error {

	a.Name = name
	a.ID = ID

	return nil
}

func (sd *SelectorData) UpdateAction(name string, ID int) error {

	//err := sd.Action.Update(name, ID)

	if err != nil {
		return err
	}

	return nil

}
