package saver

type InputAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

type ScreenShotAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

type NavigationAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

type CopyAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

type CheckAction struct {
	Name         string
	Id           int
	SelectorData SelectorData
	RuntimeData  RuntimeData
	ReturnData   ReturnData
}

type ChainMiddleAction struct {
	Name    string
	Id      int
	Actions []MiddleActionInterface
}

func (ia *InputAction) Scan() {

}

func (ia *InputAction) Value() {

}

func (ia *InputAction) SaveAction() {

}

func (ia *InputAction) GenerateSelectorDataTable() SelectorData {

	//generate table for seletor data

	return ia.SelectorData
}

func (ia *InputAction) UpdateSelectorDataTable(selector SelectorData) error {

	ia.SelectorData = selector

	return nil
}

func CreateInputAction(name string) *InputAction {
	//Generate ID

	var id int

	selectordata := SelectorData{}

	err := selectordata.UpdateAction(name, id)

	if err != nil {

	}

	outaction := InputAction{Name: name, Id: id, SelectorData: selectordata}

	return &outaction
}
