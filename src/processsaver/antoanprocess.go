package saver

type AntoanProcess struct {
	BigProcessName  string
	BigProcessID    int
	CurrentPage     string
	CurrentBrowser  string
	PreviousPage    string
	PreviousBrowser string
	AllBrowserPage  map[string][]string

	Moves []MoveInterface
}
