package player

type PlayableList interface {
	Next()
	Count()
}

type PlayableProcess interface {
	Play()
	Continue()
	Report()
}

type PlayerState interface {
	Change()
}
