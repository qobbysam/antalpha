package player

type ParentProcess struct {
	Name string
}

func (p *ParentProcess) Play() {
	// Parent Process will always navigate to url.

	//browser context. If browser already has a page. Let's reclaim the browser to run only process for current owner.

	//create a page Id

	//Navigate to URL.

	//Play next if Successful.
}

func (p *ParentProcess) Report() {

	// Update Information on report part of process

	//Proccess.Success

	//Process.Fail

}

type MiddleProcess struct {
	Name string
}

func (p *MiddleProcess) Play() {

}

func (p *MiddleProcess) Report() {

}

func (p *MiddleProcess) SetNextInfo() {

}

func (p *MiddleProcess) GetNextInfo() {

}

type EndProcess struct {
	Name string
}
