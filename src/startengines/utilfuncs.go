package startengines

import (
	"fmt"
	"log"

	"github.com/sony/sonyflake"
)

func genSonyflake() uint64 {

	//st.MachineID = MACHINEID
	flake := sonyflake.NewSonyflake(St)

	id, err := flake.NextID()

	if err != nil {
		log.Fatalf("flake.NextID() failed with %s\n", err)
	}
	// Note: this is base16, could shorten by encoding as base62 string
	fmt.Printf("github.com/sony/sonyflake:      %x\n", id)

	return id
}
