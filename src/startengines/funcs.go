package startengines

import "github.com/go-rod/rod/lib/launcher"

func CreateOneBrowser() *launcher.Launcher {
	l := launcher.New()
	return l
}

func CreatexBrowsers(number int) []*launcher.Launcher {

	var browserlist = make([]*launcher.Launcher, number)

	for i := 0; i < number; i++ {

		//l := launcher.New()

		browserlist[i] = CreateOneBrowser()

		//browserlist = append(browserlist, CreateOneBrowser())
	}

	return browserlist
}

func StartOneBrowser(browser *launcher.Launcher) string {

	u := browser.MustLaunch()

	return u

}

func StartAllBrowser(listOfBrowser []*launcher.Launcher) []string {

	var out_url_list = make([]string, len(listOfBrowser))

	for k, i := range listOfBrowser {

		out_url_list[k] = StartOneBrowser(i)

		//out_url_list = append(out_url_list, StartOneBrowser(i))
	}

	return out_url_list
}
