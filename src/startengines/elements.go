package startengines

import (
	//"github.com/go-rod/rod"
	"github.com/go-rod/rod/lib/launcher"
)

// Plan to add flags to the create process.

type ClientInfo struct {
	Id               uint64
	Device           string
	ScreenDimensions string
	WorkinDir        string
	RemotePort       string
	UserDataDir      string
}

type Engine struct {
	Id         uint64
	Url        string
	Status     bool
	LaunchInfo *launcher.Launcher
	ClientInfo ClientInfo
}
