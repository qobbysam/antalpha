package startengines

import (
	"fmt"
	"time"
)

func EndApplication(accesspoint chan string) {

	mess := "done"

	accesspoint <- mess
}

func RunForever(accesspoint chan string) {

	fmt.Println("Starting Program Now")

	for {

		select {
		case mess := <-accesspoint:

			if mess == "done" {

				fmt.Println("exiting program now from message")
				return

			}
			fmt.Println(mess)

		case <-time.After(60 * time.Second):
			fmt.Println("exiting program")
			return
		}
	}
}
