package process

type JsonReadMessage struct {
	Action   ActionType
	Value    string
	Selector string
}

func SendNavigate(inmesschan chan Process, message JsonReadMessage) {

	processToSend := NewProcess(message.Selector, message.Value, 125, message.Action, false)

	inmesschan <- processToSend

}
