package process

import (
	"fmt"

	"github.com/go-rod/rod"
)

func NewLocalRod(url string) LocalRod {

	browser := rod.New().ControlURL(url).MustConnect().NoDefaultDevice()

	outRod := LocalRod{Browser: browser, Url: url}

	return outRod

}

func NewCommTunnel(buffersize int) *ComTunnel {

	inchan := make(chan Process, buffersize)
	outchan := make(chan Process, buffersize)

	errorchan := make(chan error, buffersize)

	newcommtunnel := ComTunnel{
		InMessChan:  inchan,
		OutMessChan: outchan,
		Error:       errorchan,
	}

	// defer close(inchan)
	// defer close(outchan)
	// defer close(errorchan)
	return &newcommtunnel

}

func NewEngine(locrod LocalRod, commtunnel ComTunnel) Engine {

	out := Engine{LocalRod: locrod, ComTunnel: commtunnel, Url: locrod.Url, Islive: true}

	return out
}

func WatchEngine(engine Engine) {

	fmt.Println("watch engine started")

	go func(engine Engine) {

		for inMess := range engine.ComTunnel.InMessChan {

			action := inMess.Action.Name

			switch action {

			case Navigate:

				out_data, err := PlayActionNavigate(engine, inMess)

				if err != nil {

					ReportFail(inMess, engine.ComTunnel.Error)

				}

				ReportSuccess(out_data, engine.ComTunnel.OutMessChan)

				//case C
			}
		}
	}(engine)

	go func(engine Engine) {

		for outMess := range engine.ComTunnel.OutMessChan {

			fmt.Println(outMess)
		}

	}(engine)

	go func(engine Engine) {

		for errMess := range engine.ComTunnel.Error {

			fmt.Println(errMess)
		}

	}(engine)

}

func PlayActionNavigate(locrod Engine, process Process) (Process, error) {

	//Need to add family information on process

	//Must page from family session information.

	// page from session
	browser := locrod.LocalRod.Browser

	start_url := "https://www.twitter.com"
	page := browser.MustPage(start_url)

	err := page.Navigate(process.PayloadIn.Value)

	if err != nil {

		fmt.Println(err)

		ReportFail(process, locrod.ComTunnel.Error)
	}

	ReportSuccess(process, locrod.ComTunnel.OutMessChan)

	return Process{}, nil

}

func ReportProcessData(process Process, dataout chan Process) {

}

func ReportSuccess(process Process, outMess chan Process) {

	if process.Action.DataOut {

		ReportProcessData(process, outMess)
	}

	outMess <- process

}

func ReportFail(process Process, errchan chan error) {

}
