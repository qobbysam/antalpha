package process

func (m Report) PassMess() {

}

func NewPayLoadIn(selector string, value string) PayloadIn {

	newPayLoadIn := PayloadIn{}

	newPayLoadIn.Selector = selector
	newPayLoadIn.Value = value

	return newPayLoadIn
}

func NewPayLoadOut() PayloadOut {

	return PayloadOut{}

}

func NewAction(actiontype ActionType, outcon bool) Action {

	newAction := Action{}

	newAction.Name = actiontype
	newAction.DataOut = outcon

	return newAction
}

func NewReport(id int) Report {

	newReport := Report{}

	newReport.Id = id

	newReport.Fail = false
	newReport.Played = false
	newReport.Pass = false
	newReport.Info = ""

	return newReport
}

func NewProcess(selector string, value string, id int, actiontype ActionType, outcon bool) Process {

	action := NewAction(actiontype, outcon)

	report := NewReport(id)

	payloadin := NewPayLoadIn(selector, value)

	outProcess := Process{}

	outProcess.Action = action
	outProcess.Report = report
	outProcess.PayloadIn = payloadin

	return outProcess

}
