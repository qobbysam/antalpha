package process

type ActionType int

const (
	Navigate   ActionType = iota
	Click      ActionType = iota
	EnterText  ActionType = iota
	ScreenShot ActionType = iota
)

type Action struct {
	Name    ActionType
	DataOut bool
}

type PayloadIn struct {
	Selector string
	Value    string
	Position string
}

type PayloadOut struct {
	Html string
	Text string
	File string
}

type Report struct {
	Id     int
	Pass   bool
	Played bool
	Fail   bool
	Info   string
}

type Family struct {
	ParentBrowser string
	ParentPage    string
}

type Process struct {
	Name       string
	ID         int
	Report     Report
	Action     Action
	PayloadIn  PayloadIn
	PayloadOut PayloadOut
	Family     Family
}

// func (p *Process) UpdatePayload(payloadin PayloadIn) {

// 	 p.PayloadIn = payloadin

// }

// func (p *Process) UpdatePayloadOut(payloadout PayloadOut) {
// 	p.PayloadOut = payloadout
// }
