package process

import (
	"github.com/go-rod/rod"
)

type ComTunnel struct {
	InMessChan  chan Process
	OutMessChan chan Process
	Error       chan error
}

type LocalRod struct {
	Url     string
	Browser *rod.Browser
}

type Engine struct {
	Url      string
	Islive   bool
	LocalRod LocalRod
	//Ctx       *context.Context
	ComTunnel ComTunnel
}
