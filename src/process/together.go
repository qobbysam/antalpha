package process

import "time"

func StartProcess() {

	browser_url := "ws://localhost:10000/devtools/browser/4898d9c9-490f-4297-ba32-d408b6743eb5"

	loc_rod := NewLocalRod(browser_url)

	com_tunnel := NewCommTunnel(20)

	active_engine := NewEngine(loc_rod, *com_tunnel)

	go WatchEngine(active_engine)

	json_read := JsonReadMessage{Action: Navigate, Value: "https://www.google.com"}

	time.AfterFunc(10*time.Second, func() {
		SendNavigate(active_engine.ComTunnel.InMessChan, json_read)
	})

	time.Sleep(20 * time.Second)

}
